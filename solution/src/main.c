#include "core/modules/bmp/bmp.h"
#include "core/modules/print/print.h"
#include "core/operations/transformation/transformation.h"
#include "core/operations/validate-main/validate.h"

int main(int argc, char **argv)
{
    if (argc < 3)
    {
        print_err("Images hasn't provided, but required");
        return VALIDATION_ARGS_REQUIRED;
    }

    FILE *file_in = file_open(argv[1], RB);
    FILE *file_out = file_open(argv[2], WB);
    if (validation_file_in(file_in) != VALIDATION_OK)
    {
        print_err("Input file invalid");
        return VALIDATION_INVALID_FILE_IN;
    }
    if (validation_file_out(file_out) != VALIDATION_OK)
    {
        print_err("Output file invalid");
        return VALIDATION_INVALID_FILE_OUT;
    }

    struct image img = {0, 0, NULL};
    const enum read_status read_result = from_bmp(file_in, &img);
    if (read_result != READ_OK)
    {
        print_err("Read failed");
        return read_result;
    }

    struct image rotated_img = rotate(img);
    const enum write_status write_result = to_bmp(file_out, &rotated_img);
    if (write_result != WRITE_OK)
    {
        print_err("Write failed");
        return write_result;
    }

    img_free(img);
    img_free(rotated_img);

    file_close(file_in);
    file_close(file_out);

    return 0;
}
