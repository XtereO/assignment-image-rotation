#include "bmp.h"

static const uint32_t defaultBfType = 0x4D42;

static long bmp_padding_get(uint64_t width)
{
    return (long)(4 - (width * 3) % 4);
}

// get size image of bmp by multiple all pixels on
static uint32_t img_size_with_padding_get(struct image *img)
{
    return (sizeof(struct pixel) * img->width + bmp_padding_get(img->width)) * img->height;
}

static struct bmp_header create_bmp_header_by_img(struct image *img)
{
    uint32_t img_size = img_size_with_padding_get(img);
    return (struct bmp_header){
        .bfType = defaultBfType,
        .bfileSize = sizeof(struct bmp_header) + img_size,
        .bfReserved = 0,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = 40,
        .biHeight = img->height,
        .biWidth = img->width,
        .biPlanes = 1,
        .biBitCount = 24,
        .biCompression = 0,
        .biSizeImage = img_size,
        .biXPelsPerMeter = 0,
        .biYPelsPerMeter = 0,
        .biClrUsed = 0,
        .biClrImportant = 0,
    };
}

enum read_status from_bmp(FILE *in, struct image *img)
{
    // read bmp_header and validate
    struct bmp_header header = {0};
    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1)
    {
        return READ_INVALID_HEADER;
    }
    if (header.biBitCount != 24 || !in)
    {
        return READ_INVALID_BITS;
    }
    if (header.bfType != defaultBfType)
    {
        return READ_INVALID_SIGNATURE;
    }

    // fill pixels
    struct image new_img = img_create(header.biWidth, header.biHeight);
    const long padding = bmp_padding_get(new_img.width);
    const uint32_t img_row_size = img_row_size_get(new_img);
    for (size_t i = 0; i < new_img.height; i++)
    {
        if (fread(new_img.data + i * new_img.width, img_row_size, 1, in) != 1)
        {
            img_free(new_img);
            return READ_ROW_FAILED;
        }
        if (fseek(in, padding, SEEK_CUR))
        {
            img_free(new_img);
            return READ_PADDING_FAILED;
        }
    }
    *img = new_img;
    return READ_OK;
}

// fill image by paddings
static size_t bmp_write_padding(struct image *img, FILE *out)
{
    const uint8_t new_padding = bmp_padding_get(img->width);
    const uint8_t paddings[4] = {0};

    return fwrite(paddings, new_padding, 1, out);
}

enum write_status to_bmp(FILE *out, struct image *img)
{
    struct bmp_header header = create_bmp_header_by_img(img);
    if (fwrite(&header, sizeof(struct bmp_header), 1, out) < 1)
    {
        return WRITE_INVALID_HEADER;
    }

    // remove header offset
    if (fseek(out, header.bOffBits, SEEK_SET))
    {
        return WRITE_SEEK_HEADER_ERROR;
    };

    // fill pixels
    const uint32_t img_row_size = img_row_size_get(*img);
    if (img->data != NULL)
    {
        for (size_t i = 0; i < img->height; i++)
        {
            if (fwrite(img->data + i * img->width, img_row_size, 1, out) != 1)
            {
                return WRITE_ROW_ERROR;
            };
            if (bmp_write_padding(img, out) != 1)
            {
                return WRITE_PADDING_ERROR;
            };
        }
    }
    return WRITE_OK;
}
