#ifndef MODULE_BMP_H
#define MODULE_BMP_H
#include "../../models/bmp/bmp.h"
#include "../../models/read/read.h"
#include "../../models/write/write.h"
#include "../image/image.h"
#include <stdio.h>

enum read_status from_bmp(FILE *in, struct image *img);
enum write_status to_bmp(FILE *out, struct image *img);

#endif
