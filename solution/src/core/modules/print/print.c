#include "print.h"

void print_err(const char *const message)
{
    fprintf(stderr, "%s", message);
}

void print_out(const char *const message)
{
    fprintf(stdout, "%s", message);
}
