#ifndef MODULE_PRINT_H
#define MODULE_PRINT_H
#include <stdio.h>

void print_err(const char *const message);
void print_out(const char *const message);

#endif
