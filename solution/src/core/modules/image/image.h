#ifndef MODULE_IMAGE_H
#define MODULE_IMAGE_H
#include "../../models/image/image.h"
#include <stdlib.h>

struct image img_create(uint64_t width, uint64_t height);
void img_free(struct image img);
uint32_t img_row_size_get(struct image const img);
void img_pxl_set(struct image *img, uint64_t x, uint64_t y, struct pixel pxl);
struct pixel img_pxl_get(struct image const img, uint64_t x, uint64_t y);

#endif
