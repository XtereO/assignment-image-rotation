#include "image.h"

struct image img_create(uint64_t width, uint64_t height)
{
    struct image img = {width, height, NULL};
    img.data = malloc(width * height * sizeof(struct pixel));
    return img;
}

void img_free(struct image img)
{
    free(img.data);
}

uint32_t img_row_size_get(struct image const img)
{
    return sizeof(struct pixel) * img.width;
}

static uint64_t img_index_get(struct image const img, uint64_t x, uint64_t y)
{
    return x + img.width * y;
}
void img_pxl_set(struct image *img, uint64_t x, uint64_t y, struct pixel pxl)
{
    img->data[img_index_get(*img, x, y)] = pxl;
}
struct pixel img_pxl_get(struct image const img, uint64_t x, uint64_t y)
{
    return img.data[img_index_get(img, x, y)];
}
