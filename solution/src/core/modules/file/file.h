#ifndef MODULE_FILE_H
#define MODULE_FILE_H
#include "../../models/file/file.h"
#include <stdio.h>

FILE *file_open(const char *filename, enum file_open_mode mode);
void file_close(FILE *file);

#endif
