#include "file.h"

FILE *file_open(const char *filename, enum file_open_mode mode)
{
    const char modes[2][3] = {
        [RB] = "rb",
        [WB] = "wb"};
    return fopen(filename, modes[mode]);
}

void file_close(FILE *file)
{
    fclose(file);
}
