#include "validate.h"

enum validation_status validation_file_in(FILE *file_in)
{
    if (file_in == NULL)
    {
        return VALIDATION_INVALID_FILE_IN;
    }
    return VALIDATION_OK;
}

enum validation_status validation_file_out(FILE *file_out)
{
    if (file_out == NULL)
    {
        return VALIDATION_INVALID_FILE_OUT;
    }
    return VALIDATION_OK;
}
