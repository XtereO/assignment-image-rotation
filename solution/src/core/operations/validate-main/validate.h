#ifndef OPERATION_VALIDATE_MAIN_H
#define OPERATION_VALIDATE_MAIN_H
#include "../../models/validation/validation.h"
#include "../../modules/file/file.h"
#include <stdbool.h>
#include <stddef.h>

enum validation_status validation_file_in(FILE *file_in);
enum validation_status validation_file_out(FILE *file_out);

#endif
