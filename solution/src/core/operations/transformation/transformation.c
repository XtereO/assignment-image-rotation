#include "transformation.h"

struct image rotate(const struct image img)
{
    struct image reversed_img = img_create(img.height, img.width);
    for (size_t y = 0; y < img.height; y++)
    {
        for (size_t x = 0; x < img.width; x++)
        {
            img_pxl_set(&reversed_img, reversed_img.width - y - 1, x, img_pxl_get(img, x, y));
        }
    }
    return reversed_img;
}
