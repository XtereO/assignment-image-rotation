#ifndef OPERATION_TRANSFORMATION_H
#define OPERATION_TRANSFORMATION_H
#include "../../modules/image/image.h"
#include <stddef.h>

struct image rotate(const struct image img);

#endif
